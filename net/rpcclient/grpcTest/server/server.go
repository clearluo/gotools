package main

import (
	"context"
	"net"
	"time"

	"gitee.com/clearluo/gotools/net/rpcclient/grpcTest/proto"

	"gitee.com/clearluo/gotools/zaplog"

	"google.golang.org/grpc/credentials"

	"google.golang.org/grpc"
)

const (
	Address string = ":18100"
	Network string = "tcp"
)

type SimpleService struct{}

func (s *SimpleService) GetSimpleInfo(ctx context.Context, req *proto.SimpleRequest) (*proto.SimpleResponse, error) {
	zaplog.Infof("get from client:%v by:%v", req.Data, req.Num)
	resp := &proto.SimpleResponse{
		Code:  req.Num,
		Value: "gRPC",
	}
	//if req.Num%2 == 0 {
	//	time.Sleep(time.Second)
	//}
	time.Sleep(time.Second * 3)
	return resp, nil
}
func main() {
	listener, err := net.Listen(Network, Address)
	if err != nil {
		zaplog.Infof("net.listen err:%v", err)
		return
	}
	zaplog.Infof(Address + " net listening...")

	creds, err := credentials.NewServerTLSFromFile("./net/rpcclient/grpcTest/cert/server.crt", "./net/rpcclient/grpcTest/cert/server.key")
	if err != nil {
		zaplog.Infof("Failed to generate credentials %v", err)
		return
	}

	grpcServer := grpc.NewServer(grpc.Creds(creds))

	proto.RegisterSimpleServer(grpcServer, &SimpleService{})

	if err := grpcServer.Serve(listener); err != nil {
		zaplog.Infof("grpcTest server err:%v", err)
		return
	}
}
