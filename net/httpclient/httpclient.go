package httpclient

import (
	"net/http"
	"time"
)

func NewHttpClient(timeout time.Duration) *http.Client {
	t := http.DefaultTransport.(*http.Transport).Clone()
	t.MaxConnsPerHost = 100
	t.MaxIdleConnsPerHost = 100
	return &http.Client{
		Timeout:   timeout,
		Transport: t,
	}
}
