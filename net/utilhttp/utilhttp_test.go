package utilhttp

import (
	"testing"
	"time"
)

func TestHttpClient(t *testing.T) {
	_ = NewHttpClient()
	_ = NewHttpClient(
		ReqTimeout(time.Second*5),
		MaxIdleConns(1),
		MaxConnsPerHost(2),
		MaxIdleConnsPerHost(3),
	)
}
