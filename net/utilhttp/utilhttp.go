package utilhttp

import (
	"net/http"
	"time"
)

var (
	reqTimeoutDefault          = time.Second * 3
	maxIdleConnsDefault        = 300
	maxConnsPerHostDefault     = 1000
	maxIdleConnsPerHostDefault = 100
)

type HttpClientT struct {
	reqTimeout          time.Duration
	maxIdleConns        int
	maxConnsPerHost     int
	maxIdleConnsPerHost int
}

func NewHttpClient(options ...Option) *http.Client {
	c := &HttpClientT{
		reqTimeout:          reqTimeoutDefault,
		maxIdleConns:        maxIdleConnsDefault,
		maxConnsPerHost:     maxConnsPerHostDefault,
		maxIdleConnsPerHost: maxIdleConnsPerHostDefault,
	}
	for _, option := range options {
		option(c)
	}
	t := http.DefaultTransport.(*http.Transport).Clone()
	t.MaxIdleConns = c.maxIdleConns
	t.MaxConnsPerHost = c.maxConnsPerHost
	t.MaxIdleConnsPerHost = c.maxIdleConnsPerHost
	return &http.Client{
		Timeout:   c.reqTimeout,
		Transport: t,
	}

}
