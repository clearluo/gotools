package utilhttp

import "time"

type (
	Option func(c *HttpClientT)
)

func ReqTimeout(reqTimeout time.Duration) Option {
	return func(c *HttpClientT) {
		c.reqTimeout = reqTimeout
	}
}

func MaxIdleConns(maxIdleConns int) Option {
	return func(c *HttpClientT) {
		c.maxIdleConns = maxIdleConns
	}
}

func MaxConnsPerHost(maxConnsPerHost int) Option {
	return func(c *HttpClientT) {
		c.maxConnsPerHost = maxConnsPerHost
	}
}

func MaxIdleConnsPerHost(maxIdleConnsPerHost int) Option {
	return func(c *HttpClientT) {
		c.maxIdleConnsPerHost = maxIdleConnsPerHost
	}
}
