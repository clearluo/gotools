package zaplog

import (
	"fmt"
	"testing"
	"time"

	"go.uber.org/zap/zapcore"
)

func init() {
	logConfig := &LogConfig{
		LogDir:         "logs",
		WithMaxAge:     time.Hour * 48,
		IsPrintConsole: true,
		FileSplitTime:  time.Hour * 24,
		Level:          "debug",
	}
	Init(logConfig)
}

func TestName(t *testing.T) {
	Debug("debug")
	Info("info")
	Warn("warn")
	Error("error")
	var level zapcore.Level
	if err := level.Set("aaa"); err != nil {
		fmt.Println("level.set fail:", err)
		return
	} else {
		fmt.Println("level.set succ:", level)
	}
}
