package main

import (
	"os"
	"time"

	"gitee.com/clearluo/gotools/log"

	"gitee.com/clearluo/gotools/zaplog"

	_ "gitee.com/clearluo/gotools/db"
	_ "gitee.com/clearluo/gotools/log"
	_ "gitee.com/clearluo/gotools/util"
)

func init() {
	logDir := "logs"
	isPrintConsole := true
	os.MkdirAll(logDir, os.ModePerm)
	logConfig := &zaplog.LogConfig{
		IsPrintConsole: isPrintConsole,
		FileSplitTime:  time.Minute,
	}
	logConn := zaplog.Init(logConfig)
	_ = logConn
	log.SetLevel(log.DebugLevel)
}
func main() {
	zaplog.Debug("enter main")
	for {
		zaplog.Info(time.Now().Unix())
		time.Sleep(time.Second)
	}
}
