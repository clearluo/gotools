package pool

import (
	"bytes"
	"crypto/md5"
	"sync"
)

var (
	Md5ObjPool      *sync.Pool
	bytesBufferPool *sync.Pool
)

func init() {
	Md5ObjPool = &sync.Pool{
		New: func() interface{} {
			return md5.New()
		},
	}
	bytesBufferPool = &sync.Pool{
		New: func() interface{} {
			return &BytesBuff{
				Buffer: bytes.NewBuffer(make([]byte, 0, 1024)),
			}
		},
	}
}

type BytesBuff struct {
	*bytes.Buffer
}

func (b *BytesBuff) Free() {
	b.Reset()
	bytesBufferPool.Put(b)
}

func NewMyBytesBuff() *BytesBuff {
	return bytesBufferPool.Get().(*BytesBuff)
}
