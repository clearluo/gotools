package pool

import (
	"bytes"
	"sync"
	"testing"

	"github.com/google/uuid"
)

func BenchmarkByteBuffPool(b *testing.B) {
	wait := sync.WaitGroup{}
	for i := 0; i < b.N; i++ {
		wait.Add(1)
		go func() {
			defer wait.Done()
			logBuf := NewMyBytesBuff()
			logBuf.WriteString(uuid.New().String())
			logBuf.Free()
		}()
	}
	wait.Wait()
}

func BenchmarkStandard(b *testing.B) {
	wait := sync.WaitGroup{}
	for i := 0; i < b.N; i++ {
		wait.Add(1)
		go func() {
			defer wait.Done()
			logBuf := bytes.NewBuffer(make([]byte, 0, 1024))
			logBuf.WriteString(uuid.New().String())
		}()
	}
	wait.Wait()
}
