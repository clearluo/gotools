package qps

import (
	"sync/atomic"
	"time"

	"gitee.com/clearluo/gotools/zaplog"
)

type QPST struct {
	qps    int64
	qpm    int64
	qph    int64
	Ticker *time.Ticker
}

func NewQPS() QPST {
	return QPST{
		Ticker: time.NewTicker(time.Second),
	}
}
func (q *QPST) Add() {
	atomic.AddInt64(&q.qps, 1)
	atomic.AddInt64(&q.qpm, 1)
	atomic.AddInt64(&q.qph, 1)
}
func (q *QPST) Start() {
	for {
		select {
		case t := <-q.Ticker.C:
			if q.qps != 0 {
				zaplog.Infof("qps: %d", q.qps)
			}
			atomic.StoreInt64(&q.qps, 0)
			if t.Second() == 0 {
				if q.qpm != 0 {
					zaplog.Infof("aa qpm:%v", q.qpm)
				}
				atomic.StoreInt64(&q.qpm, 0)
			}
			if t.Minute() == 0 && t.Second() == 0 {
				zaplog.Infof("bb qpm:%v", q.qph)
				atomic.StoreInt64(&q.qph, 0)
			}
		}
	}
}
