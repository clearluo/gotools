package qps

import (
	"testing"
	"time"
)

var (
	QPS = NewQPS()
)

func TestQPS(t *testing.T) {
	go producer()
	QPS.Start()
}

func producer() {

	for {
		QPS.Add()
		time.Sleep(time.Microsecond * 1)
	}
}
