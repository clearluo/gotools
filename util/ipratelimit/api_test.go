package ipratelimit

import (
	"net"
	"testing"
	"time"

	"gitee.com/clearluo/gotools/util/ipratelimit/internal/typ"

	"github.com/spf13/cast"

	_ "net/http/pprof"

	"gitee.com/clearluo/gotools/util"
	"gitee.com/clearluo/gotools/zaplog"
)

type connMock struct {
	ipInt typ.IPInt
}

func newConnMock(ipInt typ.IPInt) *connMock {
	return &connMock{ipInt: ipInt}
}
func (c *connMock) String() string {
	return c.ipInt.String() + ":xx"
}
func (c *connMock) Network() string {
	return "tcp"
}
func (c *connMock) Read(b []byte) (n int, err error) {
	return 0, nil
}

func (c *connMock) Write(b []byte) (n int, err error) {
	return 0, nil
}
func (c *connMock) Close() error {
	return nil
}
func (c *connMock) LocalAddr() net.Addr {
	return nil
}

func (c *connMock) RemoteAddr() net.Addr {
	return c
}

func (c *connMock) SetDeadline(t time.Time) error {
	return nil
}
func (c *connMock) SetReadDeadline(t time.Time) error {
	return nil
}
func (c *connMock) SetWriteDeadline(t time.Time) error {
	return nil
}
func init() {
	zaplog.Init(&zaplog.LogConfig{
		LogDir:         "./logs",
		WithMaxAge:     0,
		IsPrintConsole: false,
		FileSplitTime:  time.Hour * 24,
		Level:          "debug",
	})
}

// go test -v -run TestPerformance
func TestPerformance(t *testing.T) {
	go util.RunPprof(":29348")
	go util.MonitorStatus(time.Second, func() string {
		return "ipPool:" + cast.ToString(ipRateLimitObj.Len())
	})
	speed := 10000
	second := 300
	start := time.Now()
	for i := 0; i < second; i++ {
		for j := 0; j < speed; j++ {
			go func(i, j int) {
				wait, ok := AllowByConn(newConnMock(typ.IPInt(uint32(i*100000 + j))))
				if ok {
					time.Sleep(time.Second * time.Duration(util.RandMN(0, 4)))
					wait.Stop()
				}
			}(i, j)
		}
		time.Sleep(time.Second)
	}
	zaplog.Infof("use:%v", time.Since(start))
	select {}
}
