package waitblack

import (
	"net"
	"time"

	"gitee.com/clearluo/gotools/util/ipratelimit/internal/typ"
)

type WaitT struct {
	Conn           net.Conn
	IPInt          typ.IPInt
	TimerAfterFunc *time.Timer
}

func (w *WaitT) Stop() {
	//zaplog.Debugf("WaitT.Stop:%v", w.IPInt.String())
	if w.TimerAfterFunc != nil {
		w.TimerAfterFunc.Stop()
	}
}
