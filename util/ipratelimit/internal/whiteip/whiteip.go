package whiteip

import (
	"sync"

	"gitee.com/clearluo/gotools/util/ipratelimit/internal/typ"
)

type WhiteIPT struct {
	IPInts map[typ.IPInt]struct{}
	sync.RWMutex
}

func NewWhiteIPT() *WhiteIPT {
	return &WhiteIPT{
		IPInts:  make(map[typ.IPInt]struct{}),
		RWMutex: sync.RWMutex{},
	}
}
func (w *WhiteIPT) List() (ipList []string) {
	w.RLock()
	for ipInt := range w.IPInts {
		ipList = append(ipList, ipInt.String())
	}
	w.RUnlock()
	return
}
func (w *WhiteIPT) IsWhite(ipInt typ.IPInt) (ok bool) {
	w.RLock()
	_, ok = w.IPInts[ipInt]
	w.RUnlock()
	return
}
func (w *WhiteIPT) Add(ipInts ...typ.IPInt) {
	w.Lock()
	for _, ipInt := range ipInts {
		w.IPInts[ipInt] = struct{}{}
	}
	w.Unlock()
}
