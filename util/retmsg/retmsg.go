package retmsg

type msg struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
}
type Message struct {
	Code int         `json:"code"`
	Msg  string      `json:"msg"`
	Data interface{} `json:"data,omitempty"`
}

func (m msg) Return() *Message {
	return &Message{
		Code: m.Code,
		Msg:  m.Msg,
	}
}

var (
	OK     = msg{0, "成功"}
	ErrSys = msg{2001, "系统错误"}
)
