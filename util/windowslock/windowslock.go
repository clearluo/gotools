package windowslock

import (
	"syscall"
)

const (
	successCallMessage = "The operation completed successfully."
)

// ScreenIsLocked 判断屏幕是否锁屏
func ScreenIsLocked() bool {
	user32 := syscall.NewLazyDLL("user32.dll")
	getForegroundWindow := user32.NewProc("GetForegroundWindow")
	activeWindowID, _, err := getForegroundWindow.Call()
	if err != nil && err.Error() != successCallMessage {
		return false
	}
	//zaplog.Warnf("activeWindowID:%v,err:%v", activeWindowID, err)
	return 0 == activeWindowID
}
