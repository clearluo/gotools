package util

import (
	"fmt"
	"testing"
)

func TestIPTranslate(t *testing.T) {
	ipString := "192.168.244.89"
	ipInt, err := IPStringToUint32(ipString)
	if err != nil {
		t.Error(err)
	}
	newIPString := IPUint32ToIP(ipInt).String()
	if ipString != newIPString {
		t.Errorf("src:%v,new:%v", ipString, newIPString)
	}
}

type Data struct {
	UserID int     `json:"userId"`
	Name   string  `json:"name"`
	Email  string  `json:"email"`
	Salary float64 `json:"salary"`
	Addr   string  `json:"addr"`
}

var (
	o *Data
)

func init() {
	o = &Data{
		UserID: 666666,
		Name:   "clearluo",
		Email:  "tolsw@qq.com",
		Salary: 666666.66,
	}
}
func BenchmarkAssertMarshal(b *testing.B) {
	for i := 0; i < b.N; i++ {
		AssertMarshal(o)
	}
}
func TestAssertMarshal(t *testing.T) {
	fmt.Println(AssertMarshal(o, 30))
	fmt.Println(AssertMarshal(`{"accountId":1909044,"timeUnix":1650600086,"token":3840882541}`, 1000))
}

func TestHideStar(t *testing.T) {
	fmt.Println("", HideStar(""))
	fmt.Println("a@qq.com", HideStar("a@qq.com"))
	fmt.Println("ab@qq.com", HideStar("ab@qq.com"))
	fmt.Println("abc@qq.com", HideStar("abc@qq.com"))
	fmt.Println("13959187513@mobile.com", HideStar("13959187513@mobile.com"))
	fmt.Println("tolsw@qq.com", HideStar("tolsw@qq.com"))
	fmt.Println("13989687123", HideStar("13989687123"))
	fmt.Println("a", HideStar("a"))
	fmt.Println("ab", HideStar("ab"))
	fmt.Println("abc", HideStar("abc"))
	fmt.Println("abcd", HideStar("abcd"))
	fmt.Println("abcde", HideStar("abcde"))
	fmt.Println("abcdef", HideStar("abcdef"))
	fmt.Println("abcdefg", HideStar("abcdefg"))
	fmt.Println("abcdefgh", HideStar("abcdefgh"))
	fmt.Println("abcdefghi", HideStar("abcdefghi"))
	fmt.Println("abcdefghifsdfsdfs", HideStar("abcdefghifsdfsdfs"))
	fmt.Println("我", HideStar("我"))
	fmt.Println("我在", HideStar("我在"))
	fmt.Println("我在干", HideStar("我在干"))
	fmt.Println("我在干什", HideStar("我在干什"))
	fmt.Println("我在干什么？", HideStar("我在干什么？"))
	fmt.Println("我在干什么？我", HideStar("我在干什么？我"))
	fmt.Println("我在干什么？我在", HideStar("我在干什么？我在"))
	fmt.Println("我在干什么？我在写", HideStar("我在干什么？我在写"))
	fmt.Println("我在干什么？我在写代", HideStar("我在干什么？我在写代"))
	fmt.Println("我在干什么？我在写代码", HideStar("我在干什么？我在写代码"))
}

func BenchmarkHideStar2(b *testing.B) {
	for i := 0; i < b.N; i++ {
		HideStar("13989687123")
	}
}

func TestRand(t *testing.T) {
	fmt.Println(RandMN(1, 10))
	fmt.Println(RandMN(1, 10))
	fmt.Println(RandMN(1, 10))
	fmt.Println(RandMN(1, 10))
	fmt.Println(RandMN(1, 10))
	fmt.Println(RandMN(1, 10))
	fmt.Println(RandMN(1, 10))
	fmt.Println(RandMN(1, 10))
	fmt.Println(RandMN(1, 10))
	fmt.Println(RandMN(1, 10))
}
func BenchmarkGetIPv4(b *testing.B) {
	for i := 0; i < b.N; i++ {
		GetIPv4()
	}
}

func BenchmarkGetLocalMac(b *testing.B) {
	for i := 0; i < b.N; i++ {
		GetLocalMac()
	}
}
