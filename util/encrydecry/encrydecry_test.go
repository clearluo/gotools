package encrydecry

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestEncryDecry(t *testing.T) {
	endeObj := NewEncryDecry("12345678")
	srcStr := "abcdefg"
	enStr := endeObj.Encryption(srcStr)
	t.Log("srcStr:", srcStr)
	t.Log("enStr:", enStr)
	assert.EqualValues(t, srcStr, endeObj.Decryption(enStr))
}
